<?php
/*
Plugin Name: TBP WP Drupal Passwords
Plugin URI: https://gitlab.com/the-bright-path/open-source/wordpress/tbp-wp-drupal-passwords
Description: Plugin for WordPress to support imported (encrypted) Drupal passwords
Version: 1.0.0
Author: Bhujagendra Ishaya, The Bright Path
Author URI: https://www.thebrightpath.com
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html
RequiresPHP: 7.4.0
*/

/**
 * @noinspection PhpUnusedParameterInspection
 */

// exit if accessed directly
use Tbp\WP\Plugin\DrupalPasswords\DrupalPasswordHasher;

if ( ! defined( 'ABSPATH' ) )
{
    http_response_code( 404 );
    exit;
}

// ignore admin heartbeats
if ( ! defined( 'TBP_IS_ADMIN_HEARTBEAT' ) )
{
    define(
        'TBP_IS_ADMIN_HEARTBEAT',
        (
            'heartbeat' === ( $_REQUEST['action'] ?? false )
            && '/wp-admin/admin-ajax.php' === $_SERVER['REQUEST_URI']
        )
    );
}

if ( TBP_IS_ADMIN_HEARTBEAT )
{
    return;
}

require_once __DIR__ . '/vendor/autoload.php';

global $wp_hasher;

if ( empty( $wp_hasher ) )
{
    $wp_hasher = new DrupalPasswordHasher( 8, true );
}
else
{

    add_filter(
        'check_password',
        /**
         * Filters whether the plaintext password matches the encrypted password.
         *
         * @since 2.5.0
         *
         * @param  bool        $check     Whether the passwords match.
         * @param  string      $password  The plaintext password.
         * @param  string      $hash      The hashed password.
         * @param  string|int  $user_id   User ID. Can be empty.
         */
        static function (
            $check,
            $password,
            $hash,
            $user_id
        ): bool {

            if ( $check )
            {
                return $check;
            }

            return DrupalPasswordHasher::Factory()
                                       ->CheckPassword( $password, $hash )
                ;
        },
        10,
        4
    );
}

// check if password needs updating
add_filter(
    'check_password',
    /**
     * Filters whether the plaintext password matches the encrypted password.
     *
     * @since 2.5.0
     *
     * @param  bool        $check     Whether the passwords match.
     * @param  string      $password  The plaintext password.
     * @param  string      $hash      The hashed password.
     * @param  string|int  $user_id   User ID. Can be empty.
     */
    static function (
        $check,
        $password,
        $hash,
        $user_id
    ): bool {

        global $wp_hasher;

        if ( ! $check )
        {
            return $check;
        }

        if ( ! $wp_hasher instanceof DrupalPasswordHasher )
        {
            return $check;
        }

        if ( ! $wp_hasher->user_needs_new_hash( $hash ) )
        {
            return $check;
        }

        // will use $wp_hasher->HashPassword( trim( $password ) )
        wp_set_password( $password, $user_id );

        return $check;
    },
    PHP_INT_MAX,
    4
);