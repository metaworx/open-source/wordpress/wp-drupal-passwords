<?php

/**
 * @file
 * Secure password hashing functions for user authentication.
 *
 * Based on the Portable PHP password hashing framework.
 * @see http://www.openwall.com/phpass/
 *
 * An alternative or custom version of this password hashing API may be
 * used by setting the variable password_inc to the name of the PHP file
 * containing replacement user_hash_password(), user_check_password(), and
 * user_needs_new_hash() functions.
 */

namespace Tbp\WP\Plugin\DrupalPasswords;

require_once ABSPATH . WPINC . '/class-phpass.php';


class DrupalPasswordHasher
    extends
    \PasswordHash
{

// constants

    /**
     * The standard log2 number of iterations for password stretching. This should
     * increase by 1 every Drupal version in order to counteract increases in the
     * speed and power of computers available to crack the hashes.
     */
    public const DRUPAL_HASH_COUNT = 15;

    /**
     * The expected (and maximum) number of characters in a hashed password.
     */
    public const DRUPAL_HASH_LENGTH = 55;

    /**
     * The maximum allowed log2 number of iterations for password stretching.
     */
    public const DRUPAL_MAX_HASH_COUNT = 30;

    /**
     * The minimum allowed log2 number of iterations for password stretching.
     */
    public const DRUPAL_MIN_HASH_COUNT = 7;

// protected properties

    protected static $instance;


    public function __construct(
        $iteration_count_log2,
        $portable_hashes
    ) {

        parent::__construct( $iteration_count_log2, $portable_hashes );

        // use drupal boundaries
        $this->iteration_count_log2 = $this->_password_enforce_log2_boundaries( $iteration_count_log2 );

        // further randomize
        $this->get_random_state();
    }


    public function get_random_bytes( $count )
    {

        static $bytes, $has_openssl;

        $missing_bytes = $count - strlen( $bytes );

        if ( $missing_bytes > 0 )
        {

            // PHP versions prior 5.3.4 experienced openssl_random_pseudo_bytes()
            // locking on Windows and rendered it unusable.
            if ( ! isset( $has_openssl ) )
            {
                $has_openssl = PHP_VERSION_ID >= 50304
                    && function_exists(
                        'openssl_random_pseudo_bytes'
                    );
            }

            $strong_result = false;

            // openssl_random_pseudo_bytes() will find entropy in a system-dependent
            // way.
            if ( $has_openssl )
            {
                $x = openssl_random_pseudo_bytes( $missing_bytes, $strong_result );
                if ( $strong_result )
                {
                    $bytes .= $x;
                }
            }

            if ( $strong_result === false && $fh = @fopen( '/dev/urandom', 'rb' ) )
            {

                // PHP only performs buffered reads, so in reality it will always read
                // at least 4096 bytes. Thus, it costs nothing extra to read and store
                // that much so as to speed any additional invocations.
                $bytes .= fread( $fh, max( 4096, $missing_bytes ) );
                fclose( $fh );
            }

            // If we couldn't get enough entropy, this simple hash-based PRNG will
            // generate a good set of pseudo-random bytes on any system.
            // Note that it may be important that our $this->random_state is passed
            // through hash() prior to being rolled into $output, that the two hash()
            // invocations are different, and that the extra input into the first one -
            // the microtime() - is prepended rather than appended. This is to avoid
            // directly leaking $this->random_state via the $output stream, which could
            // allow for trivial prediction of further "random" numbers.
            while ( strlen( $bytes ) < $count )
            {
                $bytes .= hash( 'sha256', mt_rand() . $this->get_random_state(), true );
            }
        }
        $output = substr( $bytes, 0, $count );
        $bytes  = substr( $bytes, $count );

        return $output;
    }


    public function get_random_state( $initialize = false )
    {

        if ( $initialize )
        {
            $this->random_state = $this->random_state . print_r( $_SERVER, true );
            if ( function_exists( 'getmypid' ) )
            {

                // Further initialize with the somewhat random PHP process ID.
                $this->random_state .= getmypid();
            }
        }

        return $this->random_state = hash( 'sha256', microtime() . mt_rand() . $this->random_state );
    }


    /**
     * Check whether a plain text password matches a stored hashed password.
     *
     * Alternative implementations of this function may use other data in the
     * $account object, for example the uid to look up the hash in a custom table
     * or remote database.
     *
     * @param $password
     *   A plain-text password
     * @param $stored_hash
     *   The encrypted password to verify
     *
     * @return bool
     *   TRUE or FALSE.
     */
    function CheckPassword(
        $password,
        $stored_hash
    ) {

        if ( strlen( $password ) > 4096 )
        {
            return false;
        }

        if ( substr( $stored_hash, 0, 2 ) === 'U$' )
        {

            // This may be an updated password from user_update_7000(). Such hashes
            // have 'U' added as the first character and need an extra md5().
            $stored_hash = substr( $stored_hash, 1 );
            $password    = md5( $password );
        }

        $type = substr( $stored_hash, 0, 3 );
        switch ( $type )
        {
        case '$S$':
            // A normal Drupal 7 password using sha512.
            $hash = $this->_password_crypt( 'sha512', $password, $stored_hash );
            break;

        case '$H$':
            // phpBB3 uses "$H$" for the same thing as "$P$".

        case '$P$':
            // A phpass password generated using md5.
            // This is an WordPress password or one that was imported password or from an earlier Drupal version.

            // first run the WordPress internal hasher
            if ( parent::CheckPassword( $password, $stored_hash ) )
            {
                return true;
            }

            // otherwise check old Drupal-way
            $hash = $this->_password_crypt( 'md5', $password, $stored_hash );
            break;

        default:
            return false;
        }

        return $stored_hash && $hash === $stored_hash;
    }


    /**
     * Hash a password using a secure hash.
     *
     * @param  string  $password    A plain-text password.
     * @param  int     $count_log2  Optional integer to specify the iteration count. Generally used only during
     *                              mass operations where a value less than the default is needed for speed.
     *
     * @return false|string A string containing the hashed password (and a salt), or FALSE on failure.
     */
    public function HashPassword(
        $password,
        $count_log2 = 0
    ) {

        if ( empty( $count_log2 ) )
        {

            // Use the standard iteration count.
            $count_log2 = apply_filters( 'password_count_log2', self::DRUPAL_HASH_COUNT );
        }

        return $this->_password_crypt( 'sha512', $password, $this->gensalt_private( $count_log2 ) );
    }


    /**
     * Encodes bytes into printable base 64 using the *nix standard from crypt().
     *
     * @param $input
     *   The string containing bytes to encode.
     * @param $count
     *   The number of characters (bytes) to encode.
     *
     * @return string Encoded string
     * @deprecated
     */
    function _password_base64_encode(
        $input,
        $count
    ): string {

        return $this->encode64( $input, $count );
    }


    /**
     * Hash a password using a secure stretched hash.
     *
     * By using a salt and repeated hashing the password is "stretched". Its
     * security is increased because it becomes much more computationally costly
     * for an attacker to try to break the hash by brute-force computation of the
     * hashes of a large number of plain-text words or strings to find a match.
     *
     * @param $algo
     *   The string name of a hashing algorithm usable by hash(), like 'sha256'.
     * @param $password
     *   Plain-text password up to 512 bytes (128 to 512 UTF-8 characters) to hash.
     * @param $setting
     *   An existing hash or the output of _password_generate_salt().  Must be
     *   at least 12 characters (the settings and salt).
     *
     * @return string|false
     *   A string containing the hashed password (and salt) or FALSE on failure.
     *   The return string will be truncated at DRUPAL_HASH_LENGTH characters max.
     */
    public function _password_crypt(
        $algo,
        $password,
        $setting
    ) {

        // Prevent DoS attacks by refusing to hash large passwords.
        if ( strlen( $password ) > 512 )
        {
            return false;
        }

        // The first 12 characters of an existing hash are its setting string.
        $setting = substr( $setting, 0, 12 );
        if ( $setting[0] !== '$' || $setting[2] !== '$' )
        {
            return false;
        }
        $count_log2 = $this->_password_get_count_log2( $setting );

        // Hashes may be imported from elsewhere, so we allow != DRUPAL_HASH_COUNT
        if ( $count_log2 < self::DRUPAL_MIN_HASH_COUNT || $count_log2 > self::DRUPAL_MAX_HASH_COUNT )
        {
            return false;
        }
        $salt = substr( $setting, 4, 8 );

        // Hashes must have an 8 character salt.
        if ( strlen( $salt ) !== 8 )
        {
            return false;
        }

        // Convert the base 2 logarithm into an integer.
        $count = 1 << $count_log2;

        // We rely on the hash() function being available in PHP 5.2+.
        $hash = hash( $algo, $salt . $password, true );
        do
        {
            $hash = hash( $algo, $hash . $password, true );
        }
        while ( -- $count );

        $len    = strlen( $hash );
        $output = $setting . $this->encode64( $hash, $len );

        // _password_base64_encode() of a 16 byte MD5 will always be 22 characters.
        // _password_base64_encode() of a 64 byte sha512 will always be 86 characters.
        $expected = 12 + (int) ceil( 8 * $len / 6 );

        return strlen( $output ) === $expected
            ? substr( $output, 0, self::DRUPAL_HASH_LENGTH )
            : false;
    }


    /**
     * Ensures that $count_log2 is within set bounds.
     *
     * @param $count_log2
     *   Integer that determines the number of iterations used in the hashing
     *   process. A larger value is more secure, but takes more time to complete.
     *
     * @return int
     *   Integer within set bounds that is closest to $count_log2.
     */
    public function _password_enforce_log2_boundaries( $count_log2 ): int
    {

        if ( $count_log2 < self::DRUPAL_MIN_HASH_COUNT )
        {
            return self::DRUPAL_MIN_HASH_COUNT;
        }

        if ( $count_log2 > self::DRUPAL_MAX_HASH_COUNT )
        {
            return self::DRUPAL_MAX_HASH_COUNT;
        }

        return (int) $count_log2;
    }


    /**
     * @see gensalt_private
     * @deprecated
     */
    public function _password_generate_salt( $count_log2 ): string
    {

        return $this->gensalt_private( $count_log2 );
    }


    /**
     * Parse the log2 iteration count from a stored hash or setting string.
     *
     * @param $setting string
     *
     * @return false|int
     */
    public function _password_get_count_log2( $setting )
    {

        return strpos( $this->itoa64, $setting[3] );
    }


    /**
     * Returns a string for mapping an int to the corresponding base 64 character.
     */
    public function _password_itoa64(): string
    {

        return $this->itoa64;
    }


    /**
     * Generates a random base 64-encoded salt prefixed with settings for the hash.
     *
     * Proper use of salts may defeat a number of attacks, including:
     *  - The ability to try candidate passwords against multiple hashes at once.
     *  - The ability to use pre-hashed lists of candidate passwords.
     *  - The ability to determine whether two users have the same (or different)
     *    password without actually having to guess one of the passwords.
     *
     * @param  int  $count_log2
     *   Integer that determines the number of iterations used in the hashing
     *   process. A larger value is more secure, but takes more time to complete.
     *
     * @return string A 12 character string containing the iteration count and a random salt.
     */
    public function gensalt_private( $count_log2 ): string
    {

        $output = '$S$';

        // Ensure that $count_log2 is within set bounds.
        $count_log2 = $this->_password_enforce_log2_boundaries( $count_log2 );

        // We encode the final log2 iteration count in base 64.
        $output .= $this->itoa64[ $count_log2 ];

        // 6 bytes is the standard salt for a portable phpass hash.
        $output .= $this->encode64( $this->get_random_bytes( 6 ), 6 );

        return $output;
    }


    /**
     * @param  string  $password    A plain-text password.
     * @param  int     $count_log2  Optional integer to specify the iteration count. Generally used only during
     *                              mass operations where a value less than the default is needed for speed.
     *
     * @return false|string A string containing the hashed password (and a salt), or FALSE on failure.
     * @deprecated
     */
    public function user_hash_password(
        $password,
        $count_log2 = 0
    ) {

        return $this->HashPassword( $password, $count_log2 );
    }


    /**
     * Check whether a user's hashed password needs to be replaced with a new hash.
     *
     * This is typically called during the login process when the plain text
     * password is available. A new hash is needed when the desired iteration count
     * has changed through a change in the variable password_count_log2 or
     * DRUPAL_HASH_COUNT or if the user's password hash was generated in an update
     * like user_update_7000().
     *
     * Alternative implementations of this function might use other criteria based
     * on the fields in $account.
     *
     * @param $hash
     *
     * @return bool TRUE or FALSE.
     *   TRUE or FALSE.
     */
    public function user_needs_new_hash( $hash ): bool
    {

        // Check whether this was an updated password.
        if ( substr( $hash, 0, 3 ) !== '$S$' || strlen( $hash ) !== self::DRUPAL_HASH_LENGTH )
        {
            return true;
        }

        // Ensure that $count_log2 is within set bounds.
        $count_log2 = $this->_password_enforce_log2_boundaries(
            apply_filters( 'password_count_log2', self::DRUPAL_HASH_COUNT )
        );

        // Check whether the iteration count used differs from the standard number.
        return $this->_password_get_count_log2( $hash ) !== $count_log2;
    }


    public static function Factory(
        $iteration_count_log2 = 8,
        $portable_hashes = true
    ): self {

        return self::$instance
            ?: self::$instance = new self( $iteration_count_log2, $portable_hashes );
    }

}